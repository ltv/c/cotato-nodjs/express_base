const mongoose = require('../task/connect_mongoose')

const Schema = mongoose.Schema

const userSchema = Schema({
  user_name: String,
  phone: String,
  address: String
}, { timestamps: true, collection: 'user' })

module.exports = mongoose.model('User', userSchema)
