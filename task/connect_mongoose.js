const mongoose = require('mongoose')

mongoose.connect(process.env.MONGODB_URI, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  poolSize: 10
}).then(
  () => console.log(chalk.blue('MongoDB enetviet connected! - MongoClient')),
  err => {
    console.error(err)
    console.log('%s MongoDB connection error. Please make sure MongoDB is running.')
  }
)
module.exports = mongoose
