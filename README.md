# COTATO_EXPRESS_BASE
Mô tả:
   - Cấu trúc project nodejs sử dụng express framework
   
Sử dụng
   - express generator. Link hướng dẫn: [Express application generator](https://expressjs.com/en/starter/generator.html)
   - custom lại theo yêu cầu sử dụng.
   - database: mongo sử dụng mongoose
   - nodemon
   - logger: winston
   - unit test: mocha
   
Chạy thử:
```sh
$ git clone https://gitlab.com/ltv/c/cotato-nodjs/express_base.git
$ cd express_base
$ npm install
$ touch .env
```
>copy nội dung file .env.example sang file .env và sửa MONGODB_URI='đường dẫn đến db mongo'
```sh
$ npm start
http://localhost:3000
```