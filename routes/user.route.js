const userController = require('../controllers/user.controller')

module.exports.setup = (app) => {

  //config swagger in here

  app.get('/api/get_user', userController.getUserById)
}