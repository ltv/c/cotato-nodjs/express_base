const UserService = require('../services/user.service')
const userService = new UserService()
const logger = require('../ultis/logger')
exports.getUserById = async (req, res) => {
  const userId = req.query.user_id
  try {
    //check conditions of userId
    logger.info(`========>userId: ${userId}`)
    const result = await userService.getUserById(userId)
    //TODO result
    return res.status(200).send(result)
  } catch (error) {
    //Handle error
  }
}